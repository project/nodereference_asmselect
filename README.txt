
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Usage


INTRODUCTION
------------

Author:
* Shai Yallin (electricmonk)

Module development has been sponserd by Ewave (http://www.ewave.co.il)

ASM Select (http://code.google.com/p/jquery-asmselect/) is a progressive enhancement 
to <select multiple> form elements. It provides a simpler alternative with the following
advantages:

* Easier for users to understand and interact with than regular <select multiple> form 
  elements. Users know how to interact with it sans instruction. 

* Doesn't require user to Ctrl-Click or Shift-Click multiple elements. Instead users of 
  your form can add or remove elements individually, without risk of losing those that
  have already been selected. 

* Selected elements are always visible (within the confines of your interface) while 
  unselected elements are always hidden (within a regular <select>). 

* Unlike regular <select multiple> option elements, those on asmSelect are optionally 
  sortable with drag and drop (this part requires jQuery UI). 

* asmSelect hides, maintains and updates the original <select multiple> so that no 
  changes are required to existing code. 

* If a user does not have javascript, then of course the regular <select multiple> form 
  element is used instead. 

* If the original <select multiple> form element is modified by some other jQuery or 
  javascript code, the change will be reflected in the asmSelect as well. 

This module adds a new CCK Node Reference widget option (in addition to Select and Auto
Complete) which seamlessly integrates ASM support into all Drupal forms.


INSTALLATION
------------

1. Copy the nodereference_asmselect module directory to your sites/SITENAME/modules
   directory.

2. Download ASM Select from http://code.google.com/p/jquery-asmselect/ (last known version
   to work is 1.02 beta)

3. Extract it as a sub-directory called 'asmselect' in the module folder:

     /sites/all/modules/nodereference_asmselect/asmselect/

4. Enable the module at Administer >> Site building >> Modules.


USAGE
-----

Create a CCK Node Reference field and choose ASM Select as the form widget. You can then
control the different settings of yoru widget. The JQuery UI Module
(http://drupal.org/project/jquery_ui) is required in order to use the sort feature.